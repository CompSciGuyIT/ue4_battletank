# UE4_BattleTank

An open world, arena game where tanks face off against each other for a predetermined amount of time.
The tank that has taken the least amount of damage when the time has elapsed, or is the only tank that remains,  is the winner.

**RULES**
* All tanks begin with equal armour (health) and ammunition.
* Countdown timer begins at the start of play.
* Game finishs either when only one tank remains, or when the timer reaches zero.
* The winner is the tank that has taken the least amount of damage.
* If more than one tank has equally the least amount of damage, the game is a draw.
* The environment can provide cover from enemy attacks.


**POSSIBLE ADDITIONS**
* Certain terrain is easier to across than others. Types include: 
    * Grass
    * Rock
    * Sand
    * Ice
    * Water


*  Pickups spawn randomly for a limited amount of time. Types include:
    * Increased damage
    * Extra armour
    * Increased range
    * Extra clock time
    * Increased speed (temporary)
    * Extra ammunition
    * Auto-turret (automatically fires upon enemies when within a close range to do minor damage for a short duration)
    * Invisibility (temporary)


**REQUIREMENTS**
*  Graphics
    * Tank assets
    * Projectile assets
    * Pickup assets
    * Explosion assets
    * HUD
    * Terrain assets

*  Sound
    * Tank movement
    * Shooting sounds
    * Explosion sounds
    * Pickup sounds
    * Theme music
    * Win music
    * Loss music
    * Final second’s beeps